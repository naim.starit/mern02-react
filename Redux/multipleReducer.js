const redux = require("redux");
const reduxLogger = require('redux-logger');

const createStore = redux.createStore;
const combineReducer = redux.combineReducers;
const applyMiddleware = redux.applyMiddleware;
const logger = reduxLogger.createLogger();

// action type
const BUY_MOBILE = "BUY_MOBILE";
const BUY_LAPTOP = "BUY_LAPTOP";

// action creator
function buyMobile() {
  return {
    type: BUY_MOBILE,
    info: "some data",
  };
}
function buyLaptop() {
  return {
    type: BUY_LAPTOP,
    info: "some data",
  };
}

// reducer
// (previousState, action) => newState
const initialStateMobile = {
  totalMobile: 100
};
const initialStateLaptop = {
  totalLaptop: 500,
};

const mobileReducer = (state = initialStateMobile, action) => {
  switch (action.type) {
    case BUY_MOBILE:
      return {
        ...state,
        totalMobile: state.totalMobile - 1,
      };
    default:
      return state;
  }
};

const laptopReducer = (state = initialStateLaptop, action) => {
  switch (action.type) {
    case BUY_LAPTOP:
      return {
        ...state,
        totalLaptop: state.totalLaptop - 1,
      };
    default:
      return state;
  }
};

// store
// holds application state
// to get state value, we use getState()
// to update state dispatch(action)
// subscribe(listener)

const rootReducer = combineReducer({
    mobile: mobileReducer, 
    laptop: laptopReducer
})

const store = createStore(rootReducer, applyMiddleware(logger));
console.log("Initial state", store.getState());
// const unsubscribe = store.subscribe(() =>
//    console.log("update state", store.getState())
// );
store.dispatch(buyMobile());
store.dispatch(buyMobile());
store.dispatch(buyLaptop());
store.dispatch(buyLaptop());

// unsubscribe();
