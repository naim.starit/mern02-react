const redux = require("redux");
const axios = require("axios");
const thunkMiddleware = require('redux-thunk').default;
const createStore = redux.createStore;
const applyMiddleware = redux.applyMiddleware;



// action type
const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

// reducer
// (previousState, action) => newState
const initialState = {
  loading: false, 
  posts: [],
  error: ''
};

const fetchPostRequest = () => {
    return {
        type: FETCH_POST_REQUEST
    }
}
const fetchPostSuccess = (posts) => {
    return {
        type: FETCH_POST_SUCCESS,
        payload: posts
    }
}
const fetchPostFailure = (error) => {
    return {
        type: FETCH_POST_FAILURE,
        payload: error
    }
}


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POST_REQUEST:
      return {
        loading: true
      };
      case FETCH_POST_SUCCESS: 
      return {
          loading: false,
          posts: action.payload,
          error: ''
      }
      case FETCH_POST_FAILURE: 
      return {
          loading: false,
          posts: [],
          error: action.payload
      }
    default:
      return state;
  }
};

const fetchPosts = () => {
    return function(dispatch) {
        dispatch(fetchPostRequest());
        axios.get("https://jsonplaceholder.typicode.com/posts")
        .then(res => {
            // console.log(res.data);
            const data = res.data.map(post=> post.title);
            // console.log(data);
            dispatch(fetchPostSuccess(data))
        })
        .catch(error => {
            dispatch(fetchPostFailure(error))
        })
    }
}


const store = createStore(reducer, applyMiddleware(thunkMiddleware));
console.log("Initial state", store.getState());
store.subscribe(() =>
   console.log("update state", store.getState())
);
store.dispatch(fetchPosts());
