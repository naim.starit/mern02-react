import './App.css';
import { Provider } from 'react-redux';
import store from "./redux/store";
import ParentComp from './component/ContextAPI/ParentComp';
// import Button from './component/CssComponent/Button/Button';
// import CssStyleComp from './component/CssComponent/CssStyleComp';
// import InlineCssComp from './component/CssComponent/InlineCssComp';
// import ParentComp from './component/ErrorHandling/ParentComp';
// import ControlledForm from './component/Form/ControlledForm';
// import UnControlledForm from './component/Form/UnControlledForm';
// import FragmentComp from './component/Fragment/FragmentComp';
// import TableComp from './component/Fragment/TableComp';
import OnClick from './component/HOC/OnClick';
import OnMouseOver from './component/HOC/OnMouseOver';
import CounterCustomHook1 from './component/Hooks/CustomHook/CounterCustomHook1';
import CounterCustomHook2 from './component/Hooks/CustomHook/CounterCustomHook2';
import LoginComp from './component/Hooks/CustomHook/LoginComp';
import ParentUseCallback from './component/Hooks/UseCallBack/ParentUseCallback';
import ContextParentComp from './component/Hooks/UseContext/ContextParentComp';
import UseEffectDidMountComp from './component/Hooks/UseEffect/UseEffectDidMountComp';
import MemoCounter from './component/Hooks/UseMemo/MemoCounter';
import DataFetchReducer from './component/Hooks/UseReducerHook/DataFetchReducer';
import DataFetchUsingReducer from './component/Hooks/UseReducerHook/DataFetchUsingReducer';
import MultipleUseReducer from './component/Hooks/UseReducerHook/MultipleUseReducer';
import ParentUseReducerWithUseContext from './component/Hooks/UseReducerHook/ParentUseReducerWithUseContext';
import UseReducerComp from './component/Hooks/UseReducerHook/UseReducerComp';
import UseStateComp from './component/Hooks/UseStateComp';
import UseStateWithObjArrComp from './component/Hooks/UseStateWithObjArrComp';
import UseStateWithPrevStateCopm from './component/Hooks/UseStateWithPrevStateCopm';
import AllPost from './component/HttpRequest/AllPost';
import ProductComp from './component/reduxcomp/ProductComp';
import ClickCounter from './component/RenderProps/ClickCounter';
import CountingComp from './component/RenderProps/CountingComp';
import HoverCounter from './component/RenderProps/HoverCounter';
import Product from './component/RenderProps/Product';
import AnotherProductComp from './component/reduxcomp/hook/AnotherProductComp';
import PostReduxComp from './component/reduxcomp/hook/PostReduxComp';

// import MountingComp from './component/LifeCycle/MountingComp';
// import UnmountChild from './component/LifeCycle/UnmountChild';
// import UnmountComp from './component/LifeCycle/UnmountComp';
// import UpdatingComp from './component/LifeCycle/UpdatingComp';
// import MainComp from './component/PureComp/MainComp';
// import InputRefComp from './component/Ref/InputRefComp';
// import RefsComp from './component/Ref/RefsComp';
// import ListRenderComp from './component/ListRenderComp';
// import ParentComp from './component/ParentComp';
// import PrevStateComp from './component/PrevStateComp';
// import ChildComp from './component/ChildComp';
// import LayoutComp from './component/LayoutComp';
// import StudentInfo from './component/StudentInfo';
// import Hello from './component/Hello';
// import Hi from './component/Hi';
// import MyClassComp from './component/MyClassComp';
// import WithoutJsxComp from './component/WithoutJsxComp';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        {/* <h1 className="title">Hello All</h1>
      <h3>Next Topper</h3> */}
        {/* <Hello />
      <Hi />*/}
        {/*<WithoutJsxComp /> */}
        {/* <MyClassComp name="Next" />
      <MyClassComp /> */}
        {/* <StudentInfo name="Next" age={1} />
      <StudentInfo name="Topper" age={2} />
      <StudentInfo name="Next topper" age={3} /> */}
        {/* <LayoutComp name="Next Topper">
        <h1>JavaScript Content</h1>
      </LayoutComp> */}
        {/* <StudentInfo name="Next" age={1} /> */}
        {/* <ParentComp /> */}
        {/* <PrevStateComp /> */}
        {/* <MyClassComp /> */}
        {/* <ListRenderComp /> */}
        {/* <CssStyleComp />
      <InlineCssComp />
      <Button /> */}
        {/* <ControlledForm /> */}
        {/* <UnControlledForm /> */}
        {/* <MountingComp /> */}
        {/* <UpdatingComp /> */}
        {/* <UnmountComp/> */}
        {/* <FragmentComp /> */}
        {/* <TableComp/> */}
        {/* <MainComp /> */}
        {/* <InputRefComp /> */}
        {/* <RefsComp /> */}
        {/* <ParentComp /> */}
        {/* <OnClick age='10' roll="12" />
      <OnMouseOver color="red" /> */}
        {/* <ClickCounter />
      <HoverCounter />
      <Product render={(isAvailable)=> isAvailable? "Laptop 123" : "not available"}/> */}
        {/* <CountingComp
        render={(count, handler) => (
          <ClickCounter count={count} handler={handler} />
        )}
      />
      <CountingComp
        render={(count, handler) => (
          <HoverCounter count={count} handler={handler} />
        )}
      /> */}
        {/* <ParentComp /> */}
        {/* <AllPost /> */}
        {/* <UseStateComp /> */}
        {/* <UseStateWithPrevStateCopm /> */}
        {/* <UseStateWithObjArrComp /> */}
        {/* <UseEffectDidMountComp /> */}
        {/* <ContextParentComp /> */}
        {/* <UseReducerComp /> */}
        {/* <MultipleUseReducer /> */}
        {/* <ParentUseReducerWithUseContext /> */}
        {/* <DataFetchReducer /> */}
        {/* <DataFetchUsingReducer /> */}
        {/* <LoginComp /> */}
        {/* <ParentUseCallback /> */}
        {/* <MemoCounter /> */}
        {/* <CounterCustomHook1 />
      <CounterCustomHook2 /> */}
        {/* <ProductComp /> */}
        {/* <AnotherProductComp /> */} 
        <PostReduxComp />
      </div>
    </Provider>
  );
}

export default App;
