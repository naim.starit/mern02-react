import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom';
import "./App.css";
import Home from './pages';
import About from './pages/About';
import Contact from './pages/Contact';
import More from './pages/More';
import NotFound from './pages/NotFound';
import Product from './pages/Product';
import Profile from './pages/Profile';
import Terms from './pages/Terms';
export default function App() {
    return (
        <div className='app'>
           <Router>
               <nav>
                   <Link to='/'>Home</Link>
                   <Link to='/about'>About</Link>
                   <Link to='/contact'>Contact</Link>
                   <Link to='/profile'>Profile</Link>
                   <Link to='/product'>Product</Link>
               </nav>
               <Routes>
                   <Route path='/' element={<Home/>}/>
                   <Route path='about' element={<About/>}>
                       <Route path='terms' element={<Terms/>}/>
                       <Route path='more' element={<More/>}/>
                    </Route>
                   <Route path='profile/:userName' element={<Profile/>}/>
                   <Route path='contact' element={<Contact/>}/>
                   <Route path='product' element={<Product/>}/>
                   <Route path='*' element={<NotFound/>}/>
               </Routes>
           </Router>
        </div>
    )
}
