import { BUY_MOBILE } from "./mobileTypes";
export const buyMobile = (n = 1) => {
  return {
    type: BUY_MOBILE,
    payload: n
  };
};
