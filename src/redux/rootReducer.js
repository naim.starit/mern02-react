import { combineReducers } from "redux";
import mobileReducer from './mobile/mobileReducer';
import laptopReducer from './laptop/laptopReducer';
import postReducer from './post/postReducer';

const rootReducer = combineReducers({
    mobile: mobileReducer,
    laptop: laptopReducer, 
    post: postReducer
});

export default rootReducer;