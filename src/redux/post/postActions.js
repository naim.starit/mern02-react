import axios from "axios";
import { FETCH_POST_FAILURE, FETCH_POST_REQUEST, FETCH_POST_SUCCESS } from "./postTypes";

const fetchPostRequest = () => {
  return {
    type: FETCH_POST_REQUEST,
  };
};
const fetchPostSuccess = (posts) => {
  return {
    type: FETCH_POST_SUCCESS,
    payload: posts,
  };
};
const fetchPostFailure = (error) => {
  return {
    type: FETCH_POST_FAILURE,
    payload: error,
  };
};

export const fetchPosts = () => {
  return function (dispatch) {
    dispatch(fetchPostRequest());
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        // console.log(res.data);
    
        // console.log(data);
        dispatch(fetchPostSuccess(res.data));
      })
      .catch((error) => {
        dispatch(fetchPostFailure(error));
      });
  };
};
