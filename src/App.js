import React, {useEffect, useState} from "react";
import { Routes, Route, Navigate, useLocation , useNavigate} from "react-router-dom";
import "./App.css";
import Auth from "./pages/Auth";
import NotFound from "./pages/NotFound";
import DashboardComp from "./pages/DashboardComp";
import MyProfile from "./pages/MyProfile";

export default function App() {
     const navigate = useNavigate();
    const [user, setUser] = useState(null);
    const location = useLocation();
    console.log(location);

    useEffect(()=> {
        const userInfo = localStorage.getItem('user');
        userInfo && JSON.parse(userInfo) ? setUser(true) : setUser(false)
    }, [])

    useEffect(()=> {
        localStorage.setItem('user', user)
    }, [user]); 

  return (
    <div className="app">
        <Routes>
          {!user && (
            <Route
              path="auth"
              element={<Auth authenticate={() => setUser(true)} />}
            />
          )}
          {user && (
            <>
              <Route
                path="/profile"
                element={<MyProfile logout={()=>{setUser(false); navigate('/auth')}} />}
              />
              <Route path="/dashboard" element={<DashboardComp />} />
            </>
          )}
          <Route path='*' element={<NotFound />} /> 
          <Route path='/auth' element={<Navigate to={user? '/profile' : '/auth'}/>} /> 
        </Routes>
    </div>
  );
}
