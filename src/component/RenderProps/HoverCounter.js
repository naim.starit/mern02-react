import React, { Component } from 'react'

class HoverCounter extends Component {
  render() {
    return <div>
        <button onMouseOver={()=> this.props.handler(5)}>Hoverd {this.props.count} times</button>
    </div>;
  }
}

export default HoverCounter;