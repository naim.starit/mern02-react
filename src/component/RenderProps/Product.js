import React, { Component } from 'react'

class Product extends Component {
    render() {
        return (
            <div>
                <h1>{this.props.render(true)}</h1>
            </div>
        )
    }
}
export default Product;