import React from 'react';
// statefull component
class MyClassComp extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "Hello",
            lastName: "Topper",
            age: 5
        }
        // this.nameChangeHandler = this.nameChangeHandler.bind(this)
    }

    nameChangeHandler() {
      this.setState({
        name: "Next Topper"
      })
    }

    render() {
        const { name, lastName, age} = this.state;
        const { name:pName, age:pAge } = this.props;
        return (
          <div>
            <h1>My Information</h1>
            <h2>Name: {pName ? pName : name}</h2>
            <h2>Age: {pAge ? pAge : age}</h2>
            <button
              // onClick={this.nameChangeHandler.bind(this)}
              onClick= {()=> this.nameChangeHandler()}
              // onClick = {this.nameChangeHandler}
              style={{ padding: "10px" }}
            >
              Change Name
            </button>
          </div>
        );
    }
}

export default MyClassComp;