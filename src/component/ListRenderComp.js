import React from 'react'
import ShowInfo from './ShowInfo';
const ListRenderComp = () => {
    const studentList = [
        {
            id:1,
            name: "Sakib",
            age: 10
        },
        {
            id:2,
            name: "Rakib",
            age: 12
        },
        {
            id:3,
            name: "Karim",
            age: 13
        },
        {
            id:4,
            name: "Karim",
            age: 15
        }
    ]
    return (
      <div>
          <h1>=======Student List===========</h1>
        {studentList.map((ele, index) => (
          <div key={index}>
            <ShowInfo info={ele} ourClass={(index%2===0)? 'container-blue' : 'container-black'} />
            <h1>=========================</h1>
          </div>
        ))}
      </div>
    );
}

export  default ListRenderComp;