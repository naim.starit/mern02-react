import React, { Component } from 'react'
import WithCounter from './WithCounter';

class OnMouseOver extends Component {
  render() {
      const { name, count, handler, color, age } = this.props;
      console.log("on mouse over: ", this.props);
    return (
      <div>
          <h1>{name}- {count} - {color}</h1>
        <button onMouseOver={handler}>Hovered {count} times</button>
      </div>
    );
  }
}
export default WithCounter(OnMouseOver, 5);