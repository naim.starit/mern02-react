import React, { Component } from 'react'
import axios from 'axios';
import CreatePost from './CreatePost';

class AllPost extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             allPosts: []
        }
    }
    
//    componentDidMount() {
//         axios.get("https://jsonplaceholder.typicode.com/posts")
//         .then((res)=> {
//             console.log("result = ",res);
//             this.setState({
//                 allPosts: res.data
//             })
//         })
//         .catch((err)=>{
//             console.log(err);
//         })
//     }

    getPostHandler = async() =>{
        const allData = await axios.get("https://jsonplaceholder.typicode.com/posts");
        this.setState({
            allPosts: allData.data
        })
    }

    render() {
        // console.log(this.state);
        return (
            <div>
                <CreatePost/>
                <h1>All Posts</h1>
                <button onClick={this.getPostHandler}>View Posts</button>
                {
                    !!this.state.allPosts.length? (
                        <div>
                            {
                                this.state.allPosts.map(ele=> (
                                    <div key={ele.id}>
                                        <p>ID: {ele.id}</p>
                                        <p>Title: {ele.title}</p>
                                        <p>====================</p>
                                    </div>
                                ))
                            }
                            </div>
                    ):
                    (
                        <div>
                            <h1>Loding................</h1>
                            </div>
                    )
                }
            </div>
        )
    }
}
export default AllPost;