import axios from 'axios'
import React, { Component } from 'react'

class CreatePost extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             id: 1,
             title: "hello Bangladesh",
             body: 'asdlk;fj klasdfj adsklfjdilfuj lkadjflidu aldf l',
             age: 54,
             // parent
            father: "Mr. X",
            mother: "Mr. Y"
            // address 

        }
    }

    postHander = ()=> {
        const postId = 3;
        const {id, title, body, age, father, mother} = this.state;
        const parents = {
            father,
            mother
        }
        const data = {
            id,
            title, 
            body, 
            age,
            parents
        }
        axios.post(`https://jsonplaceholder.typicode.com/posts`, data).then((res)=>{
            console.log(res);
        })
        .catch(err=>{
            console.log(err);
        })
    }
    
    render() {
        return (
            <div>
                <button onClick={this.postHander}>Submit</button>
            </div>
        )
    }
}

export default CreatePost;