import React, { Component } from 'react'

class PrevStateComp extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      isLoggedin: true,
    };
  }
  incrementHandler = () => {
    // this.setState({
    //     count: this.state.count + 1
    // })

    this.setState((prevState) => ({
      count: prevState.count + 1,
    }));
  };

  toggleButton = () => {
       this.setState((prevState) => ({
         isLoggedin: !prevState.isLoggedin
       }));
  }

  incrementFiveTimesHandler = () => {
    this.incrementHandler();
    this.incrementHandler();
    this.incrementHandler();
    this.incrementHandler();
    this.incrementHandler();
  };

  render() {
    const { count, isLoggedin } = this.state;
    return (
      <div>
        <h1>Count: {count}</h1>
        <button onClick={this.incrementFiveTimesHandler}>Increment</button>
        <br />
        <button onClick={this.toggleButton}>
          {isLoggedin ? "Logout" : "Login"}
        </button>
      </div>
    );
  }
}
export default PrevStateComp;