import React, { Component } from 'react'

class ControlledForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      age: null,
      programmingLanguage: "",
      password: "",
      libraries: [],
    };
  }
  changeHandler = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  checkboxChangeHandler = (event) => {
    if (event.target.checked) {
      this.setState({
        libraries: [...this.state.libraries, event.target.value]
      });
    }
  };

  submitHandler = (event) => {
    event.preventDefault();
    console.log(this.state);
  };

  render() {
    return (
      <form>
        <div>
          <label>Name:</label>
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.changeHandler}
          />
        </div>
        <div>
          <label>Age:</label>
          <input
            type="number"
            name="age"
            value={this.state.age}
            onChange={this.changeHandler}
          />
        </div>
        <div>
          <label>Password:</label>
          <input
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.changeHandler}
          />
        </div>
        <div>
          <label>Select a language</label>
          <select
            name="programmingLanguage"
            value={this.state.programmingLanguage}
            onChange={this.changeHandler}
          >
            <option value="javascript">JavaScript</option>
            <option value="c#">C Sharp</option>
            <option value="python">Python</option>
          </select>
        </div>
        {/* <br />
        <br />
        <h1>Name: {this.state.name}</h1>
        <h1>Age: {this.state.age}</h1> */}
        <br />
        <br />
        <p>Select your preferable libraries</p>
        <input
          type="checkbox"
          name="express"
          value="express"
          onChange={this.checkboxChangeHandler}
        />
        <label>Express</label>
        <input
          type="checkbox"
          name="hapi"
          value="hapi"
          onChange={this.checkboxChangeHandler}
        />
        <label>Hapi</label>
        <input
          type="checkbox"
          name="react"
          value="react"
          onChange={this.checkboxChangeHandler}
        />
        <label>React</label>
        <input
          type="checkbox"
          name="next.js"
          value="next.js"
          onChange={this.checkboxChangeHandler}
        />
        <label>Next.js</label>
        <br />
        <button onClick={this.submitHandler}>Submit</button>
      </form>
    );
  }
}

export default ControlledForm

