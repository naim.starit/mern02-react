import React, { Component } from 'react'

class UnControlledForm extends Component {
    constructor() {
        super();
        this.nameInput = React.createRef();
        this.ageInput = React.createRef();
    }
    submitHandler = (event) => {
        event.preventDefault();
        let name, age;
        name = this.nameInput.current.value;
        age = this.ageInput.current.value;
        
        console.log(name, age);
    }
    render() {
        return (
            <div>
                <input type='text' name="name" ref={this.nameInput} />
                <input type='number' name="age" ref={this.ageInput} />
                <button onClick={this.submitHandler}>Submit</button>
            </div>
        )
    }
}
export default UnControlledForm;