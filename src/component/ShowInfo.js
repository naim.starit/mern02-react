import React from 'react'
import './style.css'
const ShowInfo = ({info, ourClass}) => {
    return (
      <div className={ourClass}>
        <h2 className="text">
          ID: {info.id} -- Name: {info.name} -- Age: {info.age}
        </h2>
      </div>
    );
}
export default ShowInfo;