import React, {useState} from 'react'

function UseStateComp() {
    const [count, setCount] = useState(1);
    const [name, setName] = useState('');

   const updateHandler = () => {
        setCount(5);
        setName("Next Topper")
    }
    return (
        <div>
            <h1>{count} - {name}</h1>
            <button onClick={updateHandler}>Update</button>
        </div>
    )
}
export default UseStateComp;