import React, { useState } from 'react'

function UseStateWithPrevStateCopm() {
    const [count, setCount] = useState(0);
    
    const incrementTenHandler = () => {
        for(let i=0; i<10; i++) {
            setCount(prevState=> prevState + 1)
        }
    }
    return (
        <div>
            <h1> Clicked {count} times </h1>
            <button onClick={()=> setCount(count + 1)}>Increment</button>
            <button onClick={()=> setCount(count - 1)}>Decrement</button>
            <button onClick={()=> setCount(0)}>Reset</button>
            <button onClick={incrementTenHandler}>Increment 10 times</button>
        </div>
    )
}
export default UseStateWithPrevStateCopm;


// useEffect
// componentDidMount 
// componentDidUpdate 
// componentWillUnmount