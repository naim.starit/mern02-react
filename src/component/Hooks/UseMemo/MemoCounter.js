import React, {useState, useMemo} from 'react'

export default function MemoCounter() {
    const [count1, setCount1] = useState(0);
    const [count2, setCount2] = useState(0);

    const incrementHandler1 = () => {
        setCount1(count1 + 1)
    }
    const incrementHandler2 = () => {
        setCount2(count2 + 1)
    }

    const calculateHandler = useMemo(() => {
        var i = 0;
        while (i < 1000000000) i++;
        return count1 === 5;
    }, [count1])
    // const decrementHandler = () => {
    //     setCount(count - 1)
    // }
    // const resetHandler = () => {
    //     setCount(0)
    // }
    return (
        <div>
            <h1> count1 - {count1} </h1>
            <button onClick={incrementHandler1}>Increment</button>
            <h2>{calculateHandler}</h2>

            <h1> count2 - {count2} </h1>
            <button onClick={incrementHandler2}>Increment</button>
            {/* <button onClick={decrementHandler}>Decrement</button>
            <button onClick={resetHandler}>Reset</button> */}
        </div>
    )
}
