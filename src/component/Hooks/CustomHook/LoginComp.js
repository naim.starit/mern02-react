import React, {useEffect, useRef} from 'react'
import useInput from './useInput';

export default function LoginComp() {
//    const [mail, setMail] = useState('');
//    const [password, setPassword] = useState('');
    const mailRef = useRef(null);

const [mail, bindMail, resetMail] = useInput('', 'text');
const [password, bindPassword, resetPassword] = useInput('', 'password');

   const submitHandler = () => {
        console.log(mail, password);
        resetMail();
        resetPassword();
   }

   useEffect(()=> {
    mailRef.current.focus()
   }, [])

    return (
        <div>
            <input ref={mailRef} {...bindMail}/> <br/>
            <input {...bindPassword}/>

             <button onClick={submitHandler}>Submit</button>
        </div>
    )
}
