import axios from 'axios';
import React, { useState, useEffect } from 'react'

export default function DataFetch() {
    const [data, setData] = useState([]);
    const [id, setId] = useState({});

    useEffect(  () => {
        // async function fetchData() {
        //     // const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
        //     // setData(res.data)
        // }
        // fetchData(); 
        // async function fetchData() {
        //     const res = await axios.get(
        //     `https://jsonplaceholder.typicode.com/posts/${id}`
        //     );
        //     setData(res.data)
        // }
        // fetchData()
       
    }, [id])

   const fetchDataHandler = async() => {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
        setData(res.data);
    }

    return (
      <div>
        {/* <button onClick={()=> fetchDataHandler()}>Show Data</button>
            {
                data.map(ele=> (
                    <h1 key={ele.id}>{ele.title}</h1>
                ))
            } */}
        <input
          type="number"
          value={id}
          onChange={(e) => setId(e.target.value)}
        />
        <button onClick={() => fetchDataHandler()}>Show Data</button>
        <h1>{data.title}</h1>
      </div>
    );
}
