import React, {useEffect, useState} from 'react'
import Coordinate from './Coordinate';
import DataFetch from './DataFetch';

function UseEffectDidMountComp() {
   const [count, setCount] = useState(0);
   const [name, setName] = useState('');
   const [show, setShow] = useState(true);
   // componentDidMount & componentDidUpdate
    // useEffect(() => {
    //     console.log('from component did mount');
    // })  

    // useEffect(() => {
    //     console.log('from component did mount');
    // }, [count])  // conditionaly update

    // useEffect(()=> {
    //     console.log('from component did mount');
    // }, [])   // componentDidUpdate off


    return (
        <div>
            <input 
              type='text'
              value={name}
              onChange={e=> setName(e.target.value)}
            />
            <h1>Hello - {count}</h1>
            <button onClick={()=> setCount(count + 1)}>Increment</button>
            <button onClick={()=> setShow(!show)}>Toggle</button>

            { show && <Coordinate /> }
            <DataFetch />
        </div>
    )
}

export default UseEffectDidMountComp;