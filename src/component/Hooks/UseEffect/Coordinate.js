import React, {useEffect, useState} from 'react'

export default function Coordinate() {
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    // const mousePosition = (event) => {
    //     console.log('hello');
    //     setX(event.clientX);
    //     setY(event.clientY);
    // }

    // useEffect(()=> {
    //     console.log('useEffect');
    //     window.addEventListener('mousemove', mousePosition)

    // unMount
    //     return () => {
    //         console.log('unmount');
    //         window.removeEventListener('mousemove', mousePosition)
    //     }
    // }, [])

    return (
        <div>
            <h1>X-{x} -- Y-{y}</h1>
        </div>
    )
}
