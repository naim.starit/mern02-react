import React, {useState, useEffect} from 'react';
import axios from 'axios';
export default function DataFetchReducer() {
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState({});
    const [error, setError] = useState('');

    useEffect(()=> {
        axios.get(`https://jsonplaceholder.typicode.com/posts/5`)
        .then(res=> {
            setLoading(false);
            setData(res.data);
        })
        .catch(err=> {
            setLoading(false); 
            setError('something wrong')
        })
    })
    return (
        <div>
            {loading? <h1>........Loading........</h1> : <h1>{data.title}</h1>} 
            {!!error && <h1>{error}</h1> }
        </div>
    )
}
