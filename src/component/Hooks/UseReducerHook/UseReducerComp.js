// import React, {useReducer} from 'react'

// const initialState = 0;

// const reducer = (state, action) => {
//     switch(action.type) {
//         case 'increment': 
//             return state + action.value;
//         case 'decrement': 
//             return state - action.value;
//         case 'reset': 
//             return initialState; 
//         default: 
//             return state;
//     }
// }

// function UseReducerComp() {
//     const [count, dispatch] = useReducer(reducer, initialState);

//     return (
//       <div>
//         <h1>{count}</h1>
//         <button onClick={() => dispatch({ type: "increment", value: 10 })}>
//           Increment
//         </button>
//         <button onClick={() => dispatch({ type: "decrement", value: 1 })}>
//           Decrement
//         </button>
//         <button onClick={() => dispatch({ type: "reset"})}>
//           {" "}
//           Reset
//         </button>
//       </div>
//     );
// }

// export default UseReducerComp;


//============================================
// complex data with useReducer

import React, { useReducer } from "react";

const initialState = {
  count1: 5,
  count2: 50
};

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return {...state, count1: state.count1 + action.value};
    case "decrement":
      return {...state, count1: state.count1 - action.value};
    case "increment1":
      return {...state, count2: state.count2 + action.value};
    case "decrement2":
      return {...state, count2: state.count2 - action.value};
    case "reset1":
      return {...state, count1: initialState.count1};
    case "reset2":
      return {...state, count2: initialState.count2};
    default:
      return state;
  }
};

function UseReducerComp() {
  const [count, dispatch] = useReducer(reducer, initialState);

  return (
    <div>
      <h1> count 1 - {count.count1}</h1>
      <button onClick={() => dispatch({ type: "increment", value: 10 })}>
        Increment
      </button>
      <button onClick={() => dispatch({ type: "decrement", value: 1 })}>
        Decrement
      </button>
      <button onClick={() => dispatch({ type: "reset1" })}> Reset</button>

      <h1> count 2 - {count.count2}</h1>
      <button onClick={() => dispatch({ type: "increment1", value: 50 })}>
        Increment
      </button>
      <button onClick={() => dispatch({ type: "decrement2", value: 10 })}>
        Decrement
      </button>
      <button onClick={() => dispatch({ type: "reset2" })}> Reset</button>
    </div>
  );
}

export default UseReducerComp;