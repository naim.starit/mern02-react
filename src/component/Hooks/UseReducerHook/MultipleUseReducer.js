import React, { useReducer } from "react";

const initialState = {
  count1: 5
};

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return { count1: state.count1 + action.value };
    case "decrement":
      return {count1: state.count1 - action.value };
    case "reset":
      return { count1: initialState.count1 };
    default:
      return state;
  }
};

function MultipleUseReducer() {
  const [count1, dispatch1] = useReducer(reducer, initialState);
  const [count2, dispatch2] = useReducer(reducer, initialState);

  return (
    <div>
      <h1> count 1 - {count1.count1}</h1>
      <button onClick={() => dispatch1({ type: "increment", value: 10 })}>
        Increment
      </button>
      <button onClick={() => dispatch1({ type: "decrement", value: 1 })}>
        Decrement
      </button>
      <button onClick={() => dispatch1({ type: "reset" })}> Reset</button>

      <h1> count 2 - {count2.count1}</h1>
      <button onClick={() => dispatch2({ type: "increment", value: 50 })}>
        Increment
      </button>
      <button onClick={() => dispatch2({ type: "decrement", value: 10 })}>
        Decrement
      </button>
      <button onClick={() => dispatch2({ type: "reset" })}> Reset</button>
    </div>
  );
}

export default MultipleUseReducer;
