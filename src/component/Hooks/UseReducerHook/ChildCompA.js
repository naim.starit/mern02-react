import React, {useContext} from 'react'
import { CountContext } from './ParentUseReducerWithUseContext';


export default function ChildCompA() {
    const  {count, dispatch} = useContext(CountContext);
    return (
      <div>
        <h1>Comp A </h1>
        <h1>Value-{count.count1}</h1>
        <button onClick={() => dispatch({type: "increment", value: 10})}>Increment</button>
        <button onClick={() => dispatch({type: "decrement", value: 5})}>Decrement</button>
      </div>
    );
}
