import React, {useContext} from 'react'
import ThemeContext from './Context/Product';
import UserContext from './Context/User'

export default function UseContextHook() {
    const {name, isAdmin} = useContext(UserContext);
    const theme = useContext(ThemeContext);
    return (
        <div>
            <h1>{name} - {theme}</h1>
        </div>
    )
}


