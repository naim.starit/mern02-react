//1. create the context
//2. Provide a context value
//3. Consume the context value

import React from "react";

const ThemeContext = React.createContext();

const ThemeProvider = ThemeContext.Provider; // variable name must be pascalecase
const ThemeConsumer = ThemeContext.Consumer;

export { ThemeProvider, ThemeConsumer };

export default ThemeContext;
