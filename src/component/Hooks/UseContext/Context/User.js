//1. create the context
//2. Provide a context value
//3. Consume the context value

import React from "react";

const UserContext = React.createContext();

const UserProvider = UserContext.Provider; // variable name must be pascalecase
const UserConsumer = UserContext.Consumer;

export { UserProvider, UserConsumer };

export default UserContext;
