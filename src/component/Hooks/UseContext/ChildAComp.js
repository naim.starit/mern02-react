import React from 'react'
import { ThemeConsumer } from './Context/Product'
import { UserConsumer } from './Context/User'

export default function ChildAComp() {
    return (
      <ThemeConsumer>
        {(theme) => (
          <UserConsumer>
            {(user) => (
              <>
                <h1>child</h1>
                <h1>Theme is {theme} </h1>
                <h1>User {user.name} </h1>
              </>
            )}
          </UserConsumer>
        )}
      </ThemeConsumer>
    );
}

 