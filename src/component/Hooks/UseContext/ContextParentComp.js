import React from 'react'
import ChildAComp from './ChildAComp'
import { ThemeProvider } from './Context/Product'
import { UserProvider } from './Context/User'
import UseContextHook from './UseContextHook';

function ContextParentComp() {
  return (
    <div>
      <ThemeProvider value="dark">
        <UserProvider value={{name: "Next", isAdmin: false}}>
            <h1>hello from context</h1>
          <ChildAComp />
          <UseContextHook/>
        </UserProvider>
      </ThemeProvider>
    </div>
  );
}
export default ContextParentComp;