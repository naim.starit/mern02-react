import React from 'react'

function Increment({handler, children}) {
    console.log(children);
    return (
        <div>
            <button onClick={handler}>{children}</button>
        </div>
    )
}

export default React.memo(Increment)