import React, { useState } from "react";

function UseStateWithObjArrComp() {
  const [name, setName] = useState({firstName: '', lastName: ''});
  const [values, setValues] = useState([]);

  const addValueHandler = () => {
      let value = Math.floor(Math.random() * 100) + 50;
      setValues([...values, value])
  }
  return (
    <div>
        <label>First Name :</label>
        <input 
         type='text'
         value={name.firstName}
         onChange={e=> setName({...name, firstName: e.target.value})}
        />
        <label>Last Name :</label>
        <input 
         type='text'
         value={name.lastName}
         onChange={e=> setName({...name, lastName: e.target.value})}
        />
      <h1> first name: {name.firstName} - last name: {name.lastName} </h1>

       {/* array =============================== */} 

       <button onClick={addValueHandler}>Add Value</button>
       {values.map((ele, ind)=> (
           <h1 key={ind}>{ele}</h1>
       ))}
    </div>
  );
}
export default UseStateWithObjArrComp;
