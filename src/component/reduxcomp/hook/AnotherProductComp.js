import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { buyLaptop } from '../../../redux/laptop/laptopActions';
import { buyMobile } from '../../../redux/mobile/mobileActions';

export default function AnotherProductComp() {
    const [number, setNumber] = useState(1);
    const {mobile, laptop} = useSelector(state=> state);
    const {totalMobile} = mobile;
    const {totalLaptop} = laptop;

    const dispatch = useDispatch();
    return (
        <div>
            <h1>From Hook</h1>
            <h1>Total Laptop: {totalLaptop}</h1>
            <button onClick={()=> dispatch(buyLaptop())}>buy</button>
            
            <h1>Total Mobile: {totalMobile}</h1>
            <input type='text' value={number} onChange={e=>setNumber(e.target.value)} />
            <button onClick={()=> dispatch(buyMobile(number))}>buy</button>
        </div>
    )
}
