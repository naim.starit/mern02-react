import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchPosts } from '../../../redux/post/postActions';

const PostReduxComp = () => {
    const {post} = useSelector(state => state);
    const dispatch = useDispatch();

    useEffect(()=> {
        dispatch(fetchPosts())
    },[])

    return (
        <div>
            <h1>All posts</h1> 
            {
                !!post.posts ? <>
                {post.posts.map(singlePost=> (
                <Fragment key={singlePost.id}>
                    <h2>{singlePost.title}</h2>
                </Fragment>
            ))}
                </> :
                <h1>loading.....</h1>
            }
            
        </div>
    )
}

export default PostReduxComp;