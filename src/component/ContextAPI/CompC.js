import React, { Component } from "react";
import { ProductConsumer } from "./Context/ProductContext";

class CompC extends Component {
  render() {
    return (
        <ProductConsumer>
         { 
            ({name,age}) => {
              return (
                <div>
                  Comp C 
                  <h1> name - {name} - age - {age}</h1>
                </div>
              )
              }
          }
        </ProductConsumer>
    );
  }
}
export default CompC;
