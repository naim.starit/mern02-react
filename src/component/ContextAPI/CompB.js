import React, { Component } from "react";
import CompC from "./CompC";
import ProductContext from "./Context/ProductContext";

class CompB extends Component {
  render() {
    return <div>
      <h1>Comp B - {this.context.name}</h1>
        <CompC />
    </div>;
  }
}

CompB.contextType = ProductContext;
export default CompB;
