//1. create the context 
//2. Provide a context value 
//3. Consume the context value

import React from 'react';

const ProductContext = React.createContext();

const ProductProvider = ProductContext.Provider;  // variable name must be pascalecase
const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };

export default ProductContext;