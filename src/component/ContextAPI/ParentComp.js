import React, { Component } from 'react'
import CompA from './CompA';
import { ProductProvider } from './Context/ProductContext';

class ParentComp extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name: "Next Topper",
             age: 2,
             color: "Black"
        }
    }
    
    render() {
        return (
          <div>
            <ProductProvider value={{name: this.state.name, age: this.state.age}}>
              <CompA />
            </ProductProvider>
          </div>
        );
    }
}
export default ParentComp;