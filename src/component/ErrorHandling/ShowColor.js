// Error handling phase methods 

// static getDerivedStateFromError
// componentDidCatch(error, info)

import React from 'react'

const ShowColor = ({color}) => {
    if (color === "Black") {
      throw new Error("This is not a valid color");
    }
    return (
        <div>
            <h1>Color is: {color}</h1>
        </div>
    )
}
export default ShowColor;