import React, { Component } from 'react'
import ErrorBoundary from './ErrorBoundary';
import ShowColor from './ShowColor'

export default class ParentComp extends Component {
    render() {
        return (
          <div>
            <ErrorBoundary>
              <ShowColor color="Blue" />
            </ErrorBoundary>
            <ErrorBoundary>
              <ShowColor color="White" />
            </ErrorBoundary>
            <ErrorBoundary>
              <ShowColor color="Black" />
            </ErrorBoundary>
          </div>
        );
    }
}
