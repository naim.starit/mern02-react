import React from 'react'

const StudentInfo = ({name, age}) => {

  const showNameHandler = (n) => {
    console.log(name, age, n);
  }

    return (
      <div>
        <h1>My Information</h1>
        <h2>Name: {name}</h2>
        <h2>Age: {age}</h2>
        <button onClick={() => showNameHandler(654765)}>Click Me</button>
        {/* <button onClick={ showNameHandler }>Click Me</button> */}
      </div>
    );
}
export default StudentInfo;

// const StudentInfo = (props) => {
//   // const {name, age} = props;
//   return (
//     <div>
//       <h1>My Information</h1>
//       <h2>Name: {name}</h2>
//       <h2>Age: {age}</h2>
//     </div>
//   );
// };