import React, { Component } from 'react'
import MountingChild from './MountingChild';

 class MountingComp extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name: "hello",
             loading: true, 
             data : []
        }
        console.log('from constructor');
    }
    
    static getDerivedStateFromProps() {
        console.log("from getDerivedStateFromProps");
        return 0;
    }

    componentDidMount() {
        console.log("from componentDidMount");
        let myData = [
            "Blue",
            "Black",
            "White"
        ]
        setTimeout(()=>{
            this.setState({
              loading: false,
              data: myData,
            });
        }, 3000)
    }

    render() {
        console.log('from render');
        return (
          <div>
            {this.state.loading ? (
              <h1>loading.........</h1>
            ) : (
              <div>{this.state.data.map((ele, ind)=>(
                  <h1 key={ind}>{ele}</h1>
              ))}</div>
            )}
            <MountingChild />
          </div>
        );
    }
}

export default MountingComp;