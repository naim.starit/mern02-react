import React, { Component } from 'react'
import UnmountChild from './UnmountChild'

class UnmountComp extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             show: true
        }
    }
    
    showHideHandler = () => {
        this.setState({
            show: !this.state.show
        })
    }
    render() {
        return (
            <div>
                {
                    this.state.show ? <UnmountChild /> :
                        <h1>Unmount parent</h1>

                }
                <button onClick={this.showHideHandler}>Click</button>
            </div>
        )
    }
}

export default UnmountComp;