import React, { Component } from 'react'

class UnmountChild extends Component {
    
  componentWillUnmount() {
    console.log("unmounted.........");
  }

  render() {
    return (
      <div>
        <h1>Hello from Unmount child</h1>
      </div>
    );
  }
}
export default UnmountChild;