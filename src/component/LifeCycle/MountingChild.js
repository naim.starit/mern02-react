import React, { Component } from 'react'

class MountingChild extends Component {
  constructor(props) {
    super(props);

    this.state = {
        name: "hello child"
    };
    console.log("from child constructor");
  }

  static getDerivedStateFromProps() {
    console.log("from child getDerivedStateFromProps");
    return 0;
  }

  componentDidMount() {
    console.log("from child componentDidMount");
  }
  render() {
      console.log("from child render");
    return <div>{this.state.name}</div>;
  }
}
export default MountingChild;