import React, { Component } from 'react'

class UpdatingComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
    console.log("I am from Constructor");
  }

  countHandler = () => {
      this.setState(prevState=> ({
          count: prevState.count + 1
      }))
  }

  static getDerivedStateFromProps() {
    console.log("from getDerivedStateFromProps");
    return 0;
  }

  getSnapshotBeforeUpdate(){
      console.log("from getSnapshotBeforeUpdate");
      return null;
  }

  componentDidUpdate() {
    console.log("from componentDidUpdate");
  }
  render() {
      console.log("from render");
    return <div>
        <h1>{this.state.count}</h1>
        <button onClick={this.countHandler}>Click Me</button>
    </div>;
  }
}
export default UpdatingComp;