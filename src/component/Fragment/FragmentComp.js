import React, { Component, Fragment } from 'react'

class FragmentComp extends Component {
    render() {
        return (
            <>
                <h1>Hello from Fragment Comp</h1>
                {
                    new Array(5).fill("hello").map((ele,ind)=>(
                        <Fragment key={ind}>
                        <p>{ind}</p>
                        <p>{ele}</p>
                        </Fragment>
                    ))
                }
            </>
        )
    }
}
export default FragmentComp;