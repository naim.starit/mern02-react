import React, { Component } from 'react'
import ChildComp from './ChildComp';

class ParentComp extends Component {
    constructor() {
        super();
        this.state = {
            name: "Hello",
            isLoggedIn: true
        }
    }

    changeNameHandler = (n) => {
        this.setState({
            name: n,
        })
    }

    render() {
        const { isLoggedIn, name } = this.state;

        return (
          <div>
            <h1>ksdeoujoiujoi </h1>
            <h1>ksdeoujoiujoi</h1>
            <h1>ksdeoujoiujoi</h1>
            {isLoggedIn && <h1>Welcome {name}</h1>}
          </div>
        );
        //=============================================
        // return (
        //    isLoggedIn ? <h1>Welcome {name}</h1> :
        //     <h1>You are not authenticated user. Please login</h1>
        // );

        //===========================================
        // let message;
        // if(this.state.isLoggedIn) {
        //     message = <h1>Welcome {this.state.name}</h1>
        // }
        // else {
        //     message = <h1>You are not authenticated user. Please login</h1>
        // }

        // return <div>{message}</div>

        // if/else======================
        // if(this.state.isLoggedIn) {
        //     return (
        //         <h1>Welcome {this.state.name}</h1>
        //     )
        // }
        // else {
        //     return(
        //         <h1>You are not authenticated user. Please login</h1>
        //     )
        // }

        // return (
        //   <div>
        //     <h1>My name is: {this.state.name}</h1>
        //     <ChildComp changeNameHandler={this.changeNameHandler} />
        //   </div>
        // );
    }
}

export default ParentComp;


// if/else 
// element variables
// ternary 
// short circuit 