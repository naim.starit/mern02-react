//3. css modules
import React from 'react'
import Styles from './Button.module.css';

const Button = () => {
    return (
        <div>
            <button className={Styles.button}>Click Me</button>
            <h2 className={Styles.description}>Hello All</h2>
        </div>
    )
}

export default Button;