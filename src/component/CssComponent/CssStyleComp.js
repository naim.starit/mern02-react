//1. CSS Stylesheet
//2. inline style
//3. css modules
//4. CSS in libraries 

import React from 'react';
import './style.css';

function CssStyleComp() {
    return (
        <div>
            <p>CSS Stylesheet</p>
            <h1 className="title">Name: Next Topper</h1>
        </div>
    )
}

export default CssStyleComp;