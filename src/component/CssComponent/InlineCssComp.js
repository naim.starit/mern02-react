//1. CSS Stylesheet
//2. inline style
//3. css modules
//4. CSS in libraries

import React from "react";

function InlineCssComp() {
  return (
    <div>
      <p>Inline style</p>
      <h1 style={{fontSize: "35px", color: "coral"}}>Name: Next Topper</h1>
    </div>
  );
}

export default InlineCssComp;
