import React from 'react';

const WithoutJsxComp = () => {
    return (
      // <div>
      //     <h1>fefhydf</h1>
      // </div>
      React.createElement(
        "div",
        null,
        React.createElement("h1", null, "from WithoutJsxComp")
      )
    );
}
export default WithoutJsxComp;