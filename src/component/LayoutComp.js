import React from 'react'

const LayoutComp = (props) => {
    return (
      <div>
        <div style={{ color: "white", fontSize: "30px", backgroundColor: 'black' }}>
          <h1>Navbar Navbar Navbar Navbar Navbar v Navbar Navbar</h1>
        </div>
        <h1>{props.name}</h1>
        {props.children}
        <div style={{ color: "green", fontSize: "30px", backgroundColor: "black" }}>
          <h1>Footer Footer Footer Footer Footer v Footer Footer</h1>
        </div>
      </div>
    );
}

export default LayoutComp;