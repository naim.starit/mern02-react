import React, { Component } from 'react'

class InputRefComp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
    };
    this.nameInput = React.createRef();
  }

  componentDidMount() {
    this.nameInput.current.focus();
    console.log(this.nameInput);
  }
  showTextHandler =()=> {
      alert(this.nameInput.current.value)
  }
  render() {
    return (
      <div>
        <input type="text" name="name" ref={this.nameInput} />
        <button onClick={this.showTextHandler}>Show Text</button>
      </div>
    );
  }
}
export default InputRefComp;