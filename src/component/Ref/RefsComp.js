import React, { Component } from 'react'
import InputComp from './InputComp';

class RefsComp extends Component {
    constructor(props) {
        super(props)
        this.compRefs = React.createRef();
    }

    clickHandler = () => {
        this.compRefs.current.focusInput();
    }
    hello = () => {
        console.log("hello");
    }
    
    render() {
        return (
          <div>
            <InputComp ref={this.compRefs} hello={this.hello}  />
            <button onClick={this.clickHandler}>Click Me</button>
          </div>
        );
    }
}
export default RefsComp;