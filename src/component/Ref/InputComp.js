import React, { Component } from 'react'

class InputComp extends Component {
    constructor(props) {
        super(props)
        this.inpRefs = React.createRef();
    }

    focusInput = () => {
        this.inpRefs.current.focus();
        this.props.hello();
    }

    render() {
        return (
            <div>
                <input type='text' ref={this.inpRefs}/>
            </div>
        )
    }
}
export default InputComp;