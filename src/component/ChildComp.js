import React from 'react'

const ChildComp = ({changeNameHandler}) => {
    return (
      <div>
        <h1 style={{ color: "green", fontSize: "30px" }}>I am from Child</h1>
        <button onClick={()=>changeNameHandler("Sakib")}>Change my Name</button>
      </div>
    );
}

export default ChildComp;