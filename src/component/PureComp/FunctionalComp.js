import React from 'react'

const FunctionalComp = ({name, age}) => {
    console.log('functional component');
    return (
        <div>
            <h1>From functional comp {name} {age}</h1>
        </div>
    )
}

export default React.memo(FunctionalComp);