import React, { PureComponent } from "react";

class PureComp extends PureComponent {
    render() {
        console.log("Pure comp");
        return (
          <div>
            <h1>
              Pure component {this.props.name} - {this.props.age}
            </h1>
          </div>
        );
    }
}

export default PureComp;