import React from 'react'
import { Link } from 'react-router-dom'

export default function NotFound() {
    return (
        <div>
            <h1>This page is not available</h1>
            <h1>Or maybe you are not authenticate user</h1>
            <h2>Please login</h2>
            <Link to='/auth'> Login here </Link>
        </div>
    )
}
