import React from 'react'
import { Link } from 'react-router-dom'

export default function MyProfile({logout}) {
    return (
        <div>
            <h1>This is my profile</h1>
            <Link to='/dashboard'>Go to Dashboard</Link>
            <button onClick={logout}>Logout</button>
        </div>
    )
}
