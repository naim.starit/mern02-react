import React, {useState} from "react";
import { useParams, useNavigate, Navigate, useLocation } from "react-router-dom";

export default function Profile() {
    const [redirectToHome, setRedirectToHome] = useState(false)
   const params = useParams();
   const navigate = useNavigate();
   const location = useLocation();
   console.log(location);
//    const handleClick = () => navigate('/');
const handleClick = () => setRedirectToHome(true)
  return (
    <div>
      <h1>This is Profile page for {params.userName}</h1>
      <button onClick={handleClick}>Navigate to home page</button>
        {redirectToHome ? <Navigate to='/' state={{fromRoute: location}}/> : null}
    </div>
  );
}
