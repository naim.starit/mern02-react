import React from 'react'
import { useNavigate } from 'react-router-dom';

export default function Auth({authenticate}) {
    const navigate = useNavigate();
    const loginHandler = () => {
        authenticate();
        navigate('/profile');
    }
    return (
        <div>
            <h2>Please login to continue</h2>
            <button onClick={loginHandler}>Login</button>
        </div>
    )
}
