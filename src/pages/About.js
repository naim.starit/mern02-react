import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export default function About() {
    return (
        <div>
            <h1>This is About page</h1>
            <Link to='terms'>Terms</Link>
            <Link to='more'>More</Link>
            <Outlet />
        </div>
    )
}
