import React, {useEffect} from 'react'
import { useSearchParams } from 'react-router-dom';

export default function Product() {
   const [searchParams, setSearchParams] = useSearchParams();
    let min, max;
    const handleClick = () => {
        setSearchParams({
            min: 3000,
            max: 50000
        })
    }
    min = !!searchParams.get('min') ? searchParams.get('min'): 1000;
    max = !!searchParams.get("max")? searchParams.get('max'): 5000;

    useEffect(()=> {
        document.title = 'Product'
    })
    return (
        <div>
            <h1>Showing all product from price {min} to {max}</h1>
            <button onClick={handleClick}>Set Range</button>
        </div>
    )
}

